var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000;

var path = require('path');
var requestjson = require('request-json');
//var urlMovimientosMlab = "https://api.mlab.com/api/1/databases/bdbanca2ajj/collections/movimientos?apiKey=PcmRWAjyg0TLZ2XcxQhsdQxubGIrMTFX";
var urlMovimientosMlab = "https://api.mlab.com/api/1/databases/bdbanca2ajj/collections/movimientos";
var clienteMlab = requestjson.createClient( urlMovimientosMlab );

var bodyParser = require('body-parser');
app.use( bodyParser.json() );
var cors = require('cors');
app.options( '*', cors() );

const queryString = require( 'query-string' );


app.listen(port);

console.log('todo list RESTful API server started on: ' + port);

app.get( "/", function( req, res ) {
  res.sendFile( path.join( __dirname , 'index.html' ) );
} );

app.get( "/clientes/:idcliente", function( req, res ) {
  res.send( "Aqui tiene al cliente numero: " + req.params.idcliente );
} );

app.post( "/", function( req, res ){
  res.send( "Respuesta POST (Actualizado)" );
} );

app.delete( "/", function( req, res ){
  res.send( "Respuesta DELETE" );
} );

app.put( "/", function( req, res ){
  res.send( "Respuesta PUT" );
} );

// Uso de mLab (MongoDB)
function createURLMLab( query ){
  query.apiKey="PcmRWAjyg0TLZ2XcxQhsdQxubGIrMTFX";
  return "?" + queryString.stringify( query );
}

app.get( "/movimientos", function( req, res ) {
  console.log( "GET /movimientos" );
  console.log( "    query -> " + JSON.stringify( req.query ) );
  console.log( "    body -> " + JSON.stringify( req.body ) );
  clienteMlab.get( createURLMLab( req.query ), req.body, function( error, response, body ){
    console.log('error:', error);
    console.log('statusCode:', response && response.statusCode);
    console.log('body:', body);
    res.send( body );
  } );
} );

app.post( "/movimientos", function( req, res ) {
  console.log( "POST /movimientos" );
  console.log( "    query -> " + JSON.stringify( req.query ) );
  console.log( "    body -> " + JSON.stringify( req.body ) );
  clienteMlab.post( createURLMLab( req.query ), req.body, function( error, response, body ){
    console.log('error:', error);
    console.log('statusCode:', response && response.statusCode);
    console.log('body:', body);
    res.send( body );
  } );
} );

app.put( "/movimientos", function( req, res ) {
  console.log( "PUT /movimientos" );
  console.log( "    query -> " + JSON.stringify( req.query ) );
  console.log( "    body -> " + JSON.stringify( req.body ) );
  clienteMlab.put( createURLMLab( req.query ), req.body, function( error, response, body ){
    console.log('error:', error);
    console.log('statusCode:', response && response.statusCode);
    console.log('body:', body);
    res.send( body );
  } );
} );

app.delete( "/movimientos/:oid", function( req, res ) {
  console.log( "DELETE /movimientos" );
  console.log( "    query -> " + JSON.stringify( req.query ) );
  console.log( "    body -> " + JSON.stringify( req.body ) );
  console.log( "    params -> " + JSON.stringify( req.params ) );
  console.log( "delete" );
  clienteMlab.del( "/" + req.params.oid + createURLMLab( req.query ), function( error, response, body ){
  //clienteMlab.delete( v, function( error, response, body ){
    console.log('error:', error);
    console.log('statusCode:', response && response.statusCode);
    console.log('body:', body);
    res.send( body );
  } );
} );
